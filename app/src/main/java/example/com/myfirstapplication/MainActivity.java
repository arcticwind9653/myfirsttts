package example.com.myfirstapplication;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;
import java.util.Locale;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    TextToSpeech t1;
    EditText ed1;
    Button b1;
    Spinner languageSpinner;
    Spinner presetsSpinner;
    TextView lblEngine;
    String defaultTtsEngineName = "";

    public static final String DEFAULT_TTS = "com.google.android.tts";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ed1=(EditText)findViewById(R.id.editText);
        b1=(Button)findViewById(R.id.button);

        languageSpinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> languageAdapter = ArrayAdapter.createFromResource(this, R.array.languages,
                android.R.layout.simple_spinner_item);
        languageSpinner.setAdapter(languageAdapter);

        presetsSpinner = (Spinner) findViewById(R.id.spinnerPresets);
        ArrayAdapter<CharSequence> presetsAdapter = ArrayAdapter.createFromResource(this, R.array.presetSentenses,
                android.R.layout.simple_spinner_item);
        presetsSpinner.setAdapter(presetsAdapter);

        t1= new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.US);
                }
            }
        });

        List<TextToSpeech.EngineInfo> engines = t1.getEngines();
        if (engines.size() > 0) {
            defaultTtsEngineName = engines.get(0).name;
        }
        for (int i = 0; i < engines.size(); i++) {
            if (engines.get(i).name.equals(DEFAULT_TTS)) {
                //Log.d("TTS", "Found google TTS");
                defaultTtsEngineName = DEFAULT_TTS;
                t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if(status != TextToSpeech.ERROR) {
                            t1.setLanguage(Locale.US);
                        }
                    }
                }, defaultTtsEngineName);
                break;
            }
        }

        lblEngine = (TextView) findViewById(R.id.lblEngine);
        String ttsNames = "";
        for (int i = 0; i< t1.getEngines().size(); i++) {
            ttsNames = ttsNames.concat(t1.getEngines().get(i).name);
            ttsNames = ttsNames.concat("\n");
        }
        lblEngine.setText(ttsNames);

        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        t1.setLanguage(Locale.US);
                        break;
                    case 1:
                        t1.setLanguage(Locale.CHINESE);
                        break;
                    case 2:
                        t1.setLanguage(new Locale("yue", "HK"));
                        break;
                    case 3:
                        t1.setLanguage(Locale.JAPANESE);
                        break;
                    case 4:
                        t1.setLanguage(Locale.KOREAN);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                t1.setLanguage(Locale.US);
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toSpeak = "";
                if (presetsSpinner.getSelectedItem().toString().equals("<none>")) {
                    toSpeak = ed1.getText().toString();
                } else {
                    toSpeak = presetsSpinner.getSelectedItem().toString();
                }
                Toast.makeText(getApplicationContext(), toSpeak,Toast.LENGTH_SHORT).show();
                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            }
        });
    }

    public void onPause(){
        if(t1 !=null){
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }

    public void onResume() {
        super.onResume();

        ed1.setText("");

        languageSpinner.setSelection(0);
        presetsSpinner.setSelection(0);

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.US);
                }
            }
        }, defaultTtsEngineName);


    }
}
